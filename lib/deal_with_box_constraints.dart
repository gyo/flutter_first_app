import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';

class Containers extends StatelessWidget {
  const Containers({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 200,
      height: 200,
      color: Color.fromRGBO(255, 0, 0, 1),
      child: Container(
        width: 100,
        height: 100,
        color: Color.fromRGBO(0, 255, 0, 0.2),
        child: Container(
          width: 50,
          height: 50,
          color: Color.fromRGBO(0, 0, 255, 0.2),
          child: Text(
            "text",
            textDirection: TextDirection.ltr,
          ),
        ),
      ),
    );
  }
}

// この ListView は
// 縦方向 bounded constraints（maxWidth: screen size）
// 横方向 unbounded constraints（maxHeight: double.infinity）
// Row の mainAxis 方向は bounded constraints なので、その constraints 内でできるだけ小さくなる
class DealWithBoxConstraints extends StatelessWidget {
  const DealWithBoxConstraints({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return RowInHorizontalListView();
  }
}

// この ListView は
// 縦方向 unbounded constraints（maxHeight: double.infinity）
// 横方向 bounded constraints（maxWidth: screen size）
// Row の mainAxis 方向は bounded constraints なので、その constraints 内でできるだけ大きくなる
class RowInHorizontalListView extends StatelessWidget {
  const RowInHorizontalListView({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: TextDirection.ltr,
      child: Container(
        color: Color.fromRGBO(255, 127, 127, 1),
        child: ListView(
          scrollDirection: Axis.horizontal,
          children: [
            Container(
              color: Color.fromRGBO(255, 63, 63, 1),
              child: Row(
                children: [
                  Text("Hello!"),
                  Text("Hello!"),
                  Text("Hello!"),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

// エラーになる
class RowInVerticalListView extends StatelessWidget {
  const RowInVerticalListView({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: TextDirection.ltr,
      child: Container(
        color: Color.fromRGBO(255, 127, 127, 1),
        child: ListView(
          children: [
            Container(
              color: Color.fromRGBO(255, 63, 63, 1),
              child: Row(
                children: [
                  Text("Hello!"),
                  Text("Hello!"),
                  Text("Hello!"),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class UnboundedConstraints extends StatelessWidget {
  const UnboundedConstraints({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: TextDirection.ltr,
      child: ListView(
        children: [
          Container(
            width: 200,
            height: 200,
            color: Color.fromRGBO(255, 127, 127, 1),
          ),
          ListView(
            scrollDirection: Axis.horizontal,
            children: [
              Container(
                width: 200,
                height: 200,
                color: Color.fromRGBO(255, 127, 127, 1),
              ),
              Container(
                width: 200,
                height: 200,
                color: Color.fromRGBO(255, 255, 127, 1),
              ),
              Container(
                width: 200,
                height: 200,
                color: Color.fromRGBO(127, 255, 127, 1),
              ),
              Container(
                width: 200,
                height: 200,
                color: Color.fromRGBO(127, 255, 255, 1),
              ),
              Container(
                width: 200,
                height: 200,
                color: Color.fromRGBO(127, 127, 255, 1),
              ),
            ],
          ),
          Container(
            width: 200,
            height: 200,
            color: Color.fromRGBO(255, 255, 127, 1),
          ),
          Container(
            width: 200,
            height: 200,
            color: Color.fromRGBO(127, 255, 127, 1),
          ),
          Container(
            width: 200,
            height: 200,
            color: Color.fromRGBO(127, 255, 255, 1),
          ),
          Container(
            width: 200,
            height: 200,
            color: Color.fromRGBO(127, 127, 255, 1),
          ),
        ],
      ),
    );
  }
}

class WithUnconstrainedBox extends StatelessWidget {
  const WithUnconstrainedBox({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return UnconstrainedBox(
      child: Container(
        width: 200,
        height: 200,
        color: Color.fromRGBO(255, 0, 0, 1),
        child: UnconstrainedBox(
          child: Container(
            width: 100,
            height: 100,
            color: Color.fromRGBO(0, 255, 0, 0.2),
            child: UnconstrainedBox(
              child: Container(
                width: 50,
                height: 50,
                color: Color.fromRGBO(0, 0, 255, 0.2),
                child: Text(
                  "text",
                  textDirection: TextDirection.ltr,
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
