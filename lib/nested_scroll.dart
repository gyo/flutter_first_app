import 'package:flutter/material.dart';

class NestedScroll extends StatelessWidget {
  const NestedScroll({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Color.fromRGBO(255, 255, 255, 1),
      child: Directionality(
        textDirection: TextDirection.ltr,
        child: DefaultTextStyle(
          style: TextStyle(
            color: Color.fromRGBO(0, 0, 0, 0.81),
            fontWeight: FontWeight.bold,
            fontFamily: "Sans Serif",
            letterSpacing: 0.5,
            fontSize: 16,
            height: 2,
          ),
          child: _ColumnRow(),
        ),
      ),
    );
  }
}

// _MySimpleColumn > _MySimpleRow
// Expanded を使えない
class _ColumnContainerRow extends StatelessWidget {
  const _ColumnContainerRow({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return _MyColumn(
      children: [
        _MyBox(color: "blue"),
        _MyBox(
          color: "blue",
          text: "死んだら、埋めて下さい。大きな真珠貝で穴を掘って。",
        ),
        Expanded(child: _MyBox(color: "blue")),
        Container(
          height: 120.0,
          width: 360.0,
          child: _MyRow(
            children: [
              _MyBox(color: "red"),
              _MyBox(
                color: "red",
                text: "死んだら、埋めて下さい。大きな真珠貝で穴を掘って。",
              ),
              Expanded(child: _MyBox(color: "red")),
              _MyBox(color: "red"),
              // _MyBox(color: "red"),
              // _MyBox(color: "red"),
              // _MyBox(color: "red"),
              // _MyBox(color: "red"),
              // _MyBox(color: "red"),
            ],
          ),
        ),
        _MyBox(color: "blue"),
        // _MyBox(color: "blue"),
        // _MyBox(color: "blue"),
        // _MyBox(color: "blue"),
        // _MyBox(color: "blue"),
        // _MyBox(color: "blue"),
      ],
    );
  }
}

// _MyColumn > Container > _MyRow
// Expanded を使える、ただし Container で高さを固定する必要がある
class _ColumnRow extends StatelessWidget {
  const _ColumnRow({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return _MySimpleColumn(
      children: [
        _MyBox(color: "blue"),
        _MyBox(
          color: "blue",
          text: "死んだら、埋めて下さい。大きな真珠貝で穴を掘って。",
        ),
        _MyBox(color: "blue"),
        Container(
          height: 120.0,
          width: 360.0,
          child: _MySimpleRow(
            children: [
              _MyBox(color: "red"),
              _MyBox(
                color: "red",
                text: "死んだら、埋めて下さい。大きな真珠貝で穴を掘って。",
              ),
              _MyBox(color: "red"),
              _MyBox(color: "red"),
              // _MyBox(color: "red"),
              // _MyBox(color: "red"),
              // _MyBox(color: "red"),
              // _MyBox(color: "red"),
              // _MyBox(color: "red"),
            ],
          ),
        ),
        _MyBox(color: "blue"),
        // _MyBox(color: "blue"),
        // _MyBox(color: "blue"),
        // _MyBox(color: "blue"),
        // _MyBox(color: "blue"),
        // _MyBox(color: "blue"),
      ],
    );
  }
}

class _MyBox extends StatelessWidget {
  final String text;
  final String color;

  const _MyBox({
    Key key,
    this.text = "Box",
    this.color = "",
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: color == "red"
          ? Color.fromRGBO(255, 192, 192, 1.0)
          : color == "blue"
              ? Color.fromRGBO(192, 192, 255, 1.0)
              : Color.fromRGBO(192, 192, 192, 1.0),
      padding: EdgeInsets.all(16.0),
      child: Text(text),
    );
  }
}

class _MyColumn extends StatelessWidget {
  final List<Widget> children;

  const _MyColumn({
    Key key,
    this.children,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (
        BuildContext context,
        BoxConstraints parentConstraints,
      ) {
        return SingleChildScrollView(
          scrollDirection: Axis.vertical,
          child: ConstrainedBox(
            constraints: BoxConstraints(
              minHeight: parentConstraints.maxHeight,
            ),
            child: IntrinsicHeight(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: children,
              ),
            ),
          ),
        );
      },
    );
  }
}

class _MyRow extends StatelessWidget {
  final List<Widget> children;

  const _MyRow({
    Key key,
    this.children,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (
        BuildContext context,
        BoxConstraints parentConstraints,
      ) {
        return SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          child: ConstrainedBox(
            constraints: BoxConstraints(
              minWidth: parentConstraints.maxWidth,
            ),
            child: IntrinsicWidth(
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: children,
              ),
            ),
          ),
        );
      },
    );
  }
}

class _MySimpleColumn extends StatelessWidget {
  final List<Widget> children;

  const _MySimpleColumn({
    Key key,
    this.children,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      scrollDirection: Axis.vertical,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: children,
      ),
    );
  }
}

class _MySimpleRow extends StatelessWidget {
  final List<Widget> children;

  const _MySimpleRow({
    Key key,
    this.children,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: children,
      ),
    );
  }
}
