import 'package:flutter/cupertino.dart';

class MyButton extends StatelessWidget {
  final String text;
  final Function() onPress;

  const MyButton({
    Key key,
    @required this.text,
    this.onPress,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        print("onTap");
        this.onPress();
      },
      onTapCancel: () {
        print("onTapCancel");
      },
      onTapDown: (tapDownDetails) {
        print("onTapDown");
      },
      onTapUp: (tapUpDetails) {
        print("onTapUp");
      },
      onDoubleTap: () {
        print("onDoubleTap");
      },
      onLongPress: () {
        print("onLongPress");
      },
      onLongPressStart: (longPressStartDetails) {
        print("onLongPressStart");
      },
      onLongPressMoveUpdate: (longPressMoveUpdateDetails) {
        print("onLongPressMoveUpdate");
      },
      onLongPressUp: () {
        print("onLongPressUp");
      },
      onLongPressEnd: (longPressEndDetails) {
        print("onLongPressEnd");
      },
      child: Container(
        padding: EdgeInsets.all(8),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(4),
          color: Color.fromRGBO(127, 127, 255, 0.9),
        ),
        child: Text(
          text,
          textAlign: TextAlign.left,
          style: TextStyle(
            color: Color.fromRGBO(0, 0, 0, 0.9),
          ),
        ),
      ),
    );
  }
}
