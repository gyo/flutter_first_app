import 'package:flutter/widgets.dart';

class MyAppBar extends StatelessWidget {
  final String title;

  const MyAppBar({
    Key key,
    @required this.title,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.topLeft,
      padding: EdgeInsets.symmetric(
        vertical: 16,
        horizontal: 24,
      ),
      margin: EdgeInsets.only(
        bottom: 32,
      ),
      decoration: BoxDecoration(
        color: Color.fromRGBO(255, 0, 0, 0.1),
        border: Border.all(
          color: Color.fromRGBO(255, 0, 0, 0.7),
          width: 4,
          style: BorderStyle.solid,
        ),
        borderRadius: BorderRadius.circular(16),
        boxShadow: <BoxShadow>[
          BoxShadow(
            color: Color.fromRGBO(0, 0, 0, 0.1),
            offset: Offset(0, 2),
            blurRadius: 2,
            spreadRadius: 2,
          ),
        ],
      ),
      child: Text(
        title,
        textAlign: TextAlign.left,
        textDirection: TextDirection.ltr,
        style: TextStyle(
          color: Color.fromRGBO(0, 0, 0, 0.9),
          backgroundColor: Color.fromRGBO(0, 255, 0, 0.2),
          fontSize: 24,
          fontWeight: FontWeight.bold,
          fontStyle: FontStyle.italic,
          letterSpacing: 4,
          wordSpacing: 4,
          decoration: TextDecoration.underline,
          decorationColor: Color.fromRGBO(0, 0, 255, 0.9),
          decorationThickness: 2,
          fontFamily: "Serif",
        ),
      ),
    );
  }
}
