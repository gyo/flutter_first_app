import 'package:flutter/widgets.dart';

import 'counter_display.dart';
import 'counter_incrementor.dart';
import 'my_button.dart';

class MyBottomNavigation extends StatelessWidget {
  final int count;
  final Function() increment;

  const MyBottomNavigation({
    Key key,
    @required this.count,
    @required this.increment,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        children: [
          Expanded(
            child: CounterDisplay(
              count: count,
            ),
          ),
          Expanded(
            child: CounterIncrementor(
              increment: increment,
            ),
          ),
          Row(
            textDirection: TextDirection.ltr,
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            verticalDirection: VerticalDirection.up,
            children: [
              Padding(
                padding: EdgeInsets.all(8),
                child: MyButton(
                  text: "左へ",
                  onPress: () {
                    increment();
                  },
                ),
              ),
              Column(
                children: [
                  Padding(
                    padding: EdgeInsets.all(8),
                    child: MyButton(
                      text: "上へ",
                      onPress: () {
                        increment();
                      },
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.all(8),
                    child: MyButton(
                      text: "下へ",
                      onPress: () {
                        increment();
                      },
                    ),
                  ),
                ],
              ),
              Padding(
                padding: EdgeInsets.all(8),
                child: MyButton(
                  text: "右へ",
                  onPress: () {
                    increment();
                  },
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
