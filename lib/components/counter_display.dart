import 'package:flutter/widgets.dart';

class CounterDisplay extends StatelessWidget {
  const CounterDisplay({
    Key key,
    @required this.count,
  }) : super(key: key);

  final int count;

  @override
  Widget build(BuildContext context) {
    return Text(
      "Count: $count",
      textDirection: TextDirection.ltr,
    );
  }
}
