import 'package:flutter/widgets.dart';

import 'my_button.dart';

class CounterIncrementor extends StatelessWidget {
  const CounterIncrementor({
    Key key,
    @required this.increment,
  }) : super(key: key);

  final Function() increment;

  @override
  Widget build(BuildContext context) {
    return MyButton(
      text: "+1",
      onPress: this.increment,
    );
  }
}
