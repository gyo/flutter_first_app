import 'package:flutter/material.dart';
import 'package:flutter_first_app/numbers_state_notifier.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import 'numbers_change_notifier.dart';

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      home: MyHomePage(),
      // home: MyScopedHomePage(),
    );
  }
}

final numberProvider = Provider<int>((ref) {
  return 1;
});

final numberStateProvider = StateProvider<int>((ref) {
  return 2;
});

final numbersNotifierProvider = StateNotifierProvider<NumbersNotifier>((ref) {
  return NumbersNotifier();
});

final numbersChangeNotifierProvider =
    ChangeNotifierProvider<NumbersChangeNotifier>((ref) {
  return NumbersChangeNotifier();
});

final myStreamProvider =
    StreamProvider.autoDispose.family<int, int>((ref, extraNumber) {
  return Stream.value(5 + extraNumber);
});

final myFutureProvider = FutureProvider.autoDispose<int>((ref) {
  return Future.value(6);
});

final scopedNumberProvider = ScopedProvider<int>((ref) {
  throw UnimplementedError();
});

class MyHomePage extends ConsumerWidget {
  @override
  Widget build(BuildContext context, ScopedReader watch) {
    final number = watch(numberProvider);
    final numberState = watch(numberStateProvider).state;
    final numbersNotifierState = watch(numbersNotifierProvider.state);
    final numbersChangeNotifier = watch(numbersChangeNotifierProvider);
    final futureNumber = watch(myFutureProvider);
    final streamNumber = watch(myStreamProvider(5)); // == 10

    return Scaffold(
      body: _DemoBody(
          number: number,
          numberState: numberState,
          numbersNotifierState: numbersNotifierState,
          numbersChangeNotifier: numbersChangeNotifier,
          streamNumber: streamNumber,
          futureNumber: futureNumber),
    );
  }
}

class _DemoBody extends StatelessWidget {
  const _DemoBody({
    Key key,
    @required this.number,
    @required this.numberState,
    @required this.numbersNotifierState,
    @required this.numbersChangeNotifier,
    @required this.streamNumber,
    @required this.futureNumber,
  }) : super(key: key);

  final int number;
  final int numberState;
  final List<int> numbersNotifierState;
  final NumbersChangeNotifier numbersChangeNotifier;
  final AsyncValue<int> streamNumber;
  final AsyncValue<int> futureNumber;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Text(number.toString()),
        Text(numberState.toString()),
        RaisedButton(
          child: Text("+1"),
          color: Colors.orange,
          textColor: Colors.white,
          onPressed: () => increment(context),
        ),
        Expanded(
            child: ListView.builder(
          itemBuilder: (context, index) =>
              Text(numbersNotifierState[index].toString()),
          itemCount: numbersNotifierState.length,
        )),
        Row(
          children: [
            RaisedButton(
              child: Text("add"),
              color: Colors.blue,
              textColor: Colors.white,
              onPressed: () => context.read(numbersNotifierProvider).add(3),
            ),
            RaisedButton(
              child: Text("delete"),
              color: Colors.blue,
              textColor: Colors.white,
              onPressed: () => context.read(numbersNotifierProvider).delete(3),
            ),
          ],
        ),
        Expanded(
            child: ListView.builder(
          itemBuilder: (context, index) =>
              Text(numbersChangeNotifier.numbers[index].toString()),
          itemCount: numbersChangeNotifier.numbers.length,
        )),
        Row(
          children: [
            RaisedButton(
              child: Text("add"),
              color: Colors.brown,
              textColor: Colors.white,
              onPressed: () => numbersChangeNotifier.add(4),
            ),
            RaisedButton(
              child: Text("remove"),
              color: Colors.brown,
              textColor: Colors.white,
              onPressed: () => numbersChangeNotifier.removeLast(),
            ),
          ],
        ),
        Container(
          child: streamNumber.when(
            data: (data) => Text(data.toString()),
            loading: () => CircularProgressIndicator(),
            error: (e, s) => SizedBox(),
          ),
        ),
        Container(
          child: futureNumber.when(
            data: (data) => Text(data.toString()),
            loading: () => CircularProgressIndicator(),
            error: (e, s) => SizedBox(),
          ),
        ),
      ],
    );
  }

  void increment(BuildContext context) {
    context.read(numberStateProvider).state++;
  }
}

class MyScopedHomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ProviderScope(
      overrides: [scopedNumberProvider.overrideWithValue(7)],
      child: DetailsPage(),
    );
  }
}

class DetailsPage extends ConsumerWidget {
  @override
  Widget build(BuildContext context, ScopedReader watch) {
    final myScopedNumber = watch(scopedNumberProvider);
    return Container(child: Text(myScopedNumber.toString()));
  }
}
