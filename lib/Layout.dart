import 'package:flutter/widgets.dart';

class Layout extends StatelessWidget {
  const Layout({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: TextDirection.ltr,
      child: Center(
        child: Container(
          height: 240,
          width: 240,
          decoration: BoxDecoration(
            color: Color.fromRGBO(255, 127, 127, 1),
          ),
          child: StackDemo(),
        ),
      ),
    );
  }
}

class AlignDemo extends StatelessWidget {
  const AlignDemo({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Align(
        alignment: Alignment.centerRight,
        heightFactor: 2,
        widthFactor: 2,
        child: Container(
          height: 30,
          width: 30,
          decoration: BoxDecoration(
            color: Color.fromRGBO(255, 0, 0, 1),
          ),
        ),
      ),
    );
  }
}

class AspectRatioDemo extends StatelessWidget {
  const AspectRatioDemo({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: AspectRatio(
        aspectRatio: 16 / 9,
        child: Container(
          decoration: BoxDecoration(
            color: Color.fromRGBO(255, 0, 0, 1),
          ),
        ),
      ),
    );
  }
}

class BaselineDemo extends StatelessWidget {
  const BaselineDemo({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Baseline(
      baseline: 0,
      baselineType: TextBaseline.alphabetic,
      child: Text(
        "╋ こんにちは my ipsum",
        style: TextStyle(
          color: Color.fromRGBO(255, 0, 0, 1),
          fontSize: 20,
          fontFamily: "Sans Serif",
        ),
      ),
    );
  }
}

class CenterDemo extends StatelessWidget {
  const CenterDemo({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Center(
        heightFactor: 2,
        widthFactor: 2,
        child: Container(
          height: 30,
          width: 30,
          decoration: BoxDecoration(
            color: Color.fromRGBO(255, 0, 0, 1),
          ),
        ),
      ),
    );
  }
}

class ConstrainedBoxDemo extends StatelessWidget {
  const ConstrainedBoxDemo({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        children: [
          ConstrainedBox(
            constraints: BoxConstraints(
              minWidth: 30,
              maxWidth: 90,
              minHeight: 30,
              maxHeight: 90,
            ),
            child: Text(
              "L",
              style: TextStyle(
                color: Color.fromRGBO(255, 0, 0, 1),
                fontSize: 12,
              ),
            ),
          ),
          Container(
            height: 10,
            width: 240,
            decoration: BoxDecoration(
              color: Color.fromRGBO(255, 255, 255, 1),
            ),
          ),
          ConstrainedBox(
            constraints: BoxConstraints(
              minWidth: 30,
              maxWidth: 90,
              minHeight: 30,
              maxHeight: 90,
            ),
            child: Text(
              "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
              style: TextStyle(
                color: Color.fromRGBO(255, 0, 0, 1),
                fontSize: 12,
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class ContainerDemo extends StatelessWidget {
  const ContainerDemo({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        alignment: Alignment.bottomRight,
        padding: EdgeInsets.all(16),
        margin: EdgeInsets.all(16),
        height: 120,
        width: 120,
        child: Container(
          height: 30,
          width: 30,
          decoration: BoxDecoration(
            color: Color.fromRGBO(255, 0, 0, 1),
          ),
        ),
      ),
    );
  }
}

class FittedBoxDemo extends StatelessWidget {
  const FittedBoxDemo({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        height: 60,
        width: 120,
        child: FittedBox(
          fit: BoxFit.contain,
          alignment: Alignment.center,
          clipBehavior: Clip.hardEdge,
          child: Container(
            height: 90,
            width: 90,
            decoration: BoxDecoration(
              color: Color.fromRGBO(255, 0, 0, 1),
            ),
          ),
        ),
      ),
    );
  }
}

class FractionallySizedBoxDemo extends StatelessWidget {
  const FractionallySizedBoxDemo({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: FractionallySizedBox(
        widthFactor: 0.5,
        heightFactor: 0.5,
        child: Container(
          decoration: BoxDecoration(
            color: Color.fromRGBO(255, 0, 0, 1),
          ),
        ),
      ),
    );
  }
}

class OffstageDemo extends StatelessWidget {
  const OffstageDemo({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Offstage(
        child: Container(
          height: 30,
          width: 30,
          decoration: BoxDecoration(
            color: Color.fromRGBO(255, 0, 0, 1),
          ),
        ),
      ),
    );
  }
}

class OverflowBoxDemo extends StatelessWidget {
  const OverflowBoxDemo({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: OverflowBox(
        minHeight: 120,
        maxWidth: 270,
        child: Container(
          height: 30,
          width: 480,
          decoration: BoxDecoration(
            color: Color.fromRGBO(255, 0, 0, 1),
          ),
        ),
      ),
    );
  }
}

class SizedBoxDemo extends StatelessWidget {
  const SizedBoxDemo({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: SizedBox(
        height: 30,
        width: 30,
        child: Container(
          decoration: BoxDecoration(
            color: Color.fromRGBO(255, 0, 0, 1),
          ),
        ),
      ),
    );
  }
}

class StackDemo extends StatelessWidget {
  const StackDemo({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: Alignment.bottomLeft,
      fit: StackFit.loose,
      overflow: Overflow.visible,
      clipBehavior: Clip.hardEdge,
      children: [
        Container(
          width: 120,
          height: 120,
          color: Color.fromRGBO(63, 0, 0, 1),
        ),
        Container(
          width: 90,
          height: 90,
          color: Color.fromRGBO(127, 0, 0, 1),
        ),
        Container(
          width: 60,
          height: 60,
          color: Color.fromRGBO(255, 0, 0, 1),
        ),
        Positioned(
          bottom: -10,
          right: -10,
          child: Container(
            width: 30,
            height: 30,
            color: Color.fromRGBO(0, 127, 0, 1),
          ),
        ),
      ],
    );
  }
}

class TransformDemo extends StatelessWidget {
  const TransformDemo({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Transform(
        transform: Matrix4.skewY(0.3)..rotateZ(30),
        child: Container(
          height: 30,
          width: 30,
          decoration: BoxDecoration(
            color: Color.fromRGBO(255, 0, 0, 1),
          ),
        ),
      ),
    );
  }
}
