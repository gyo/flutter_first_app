import 'package:flutter/material.dart';

import 'components/my_app_bar.dart';
import 'components/my_bottomnavigation.dart';

class TenDreamingNights extends StatefulWidget {
  const TenDreamingNights({Key key}) : super(key: key);

  @override
  _TenDreamingNightsState createState() => _TenDreamingNightsState();
}

class _TenDreamingNightsState extends State<TenDreamingNights> {
  int _counter = 0;

  void _increment() {
    setState(() {
      ++_counter;
    });
  }

  final descTextStyle = TextStyle(
    color: Color.fromRGBO(0, 0, 127, 0.81),
    fontWeight: FontWeight.normal,
    fontFamily: "Sans Serif",
    letterSpacing: 0.5,
    fontSize: 16,
    height: 1.7,
  );

  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: TextDirection.ltr,
      child: Container(
        color: Color.fromRGBO(255, 255, 255, 1),
        child: DefaultTextStyle.merge(
          style: descTextStyle,
          child: ListView(
            children: [
              MyAppBar(
                title: "Example Title",
              ),
              Column(
                children: [
                  Text("第一夜"),
                  Text("こんな夢を見た。"),
                  Text(
                      "腕組をして枕元に坐っていると、仰向に寝た女が、静かな声でもう死にますと云う。女は長い髪を枕に敷いて、輪郭の柔らかな瓜実顔をその中に横たえている。真白な頬の底に温かい血の色がほどよく差して、唇の色は無論赤い。とうてい死にそうには見えない。しかし女は静かな声で、もう死にますと判然云った。自分も確にこれは死ぬなと思った。そこで、そうかね、もう死ぬのかね、と上から覗き込むようにして聞いて見た。死にますとも、と云いながら、女はぱっちりと眼を開けた。大きな潤のある眼で、長い睫に包まれた中は、ただ一面に真黒であった。その真黒な眸の奥に、自分の姿が鮮に浮かんでいる。"),
                  Text(
                      "自分は透き徹るほど深く見えるこの黒眼の色沢を眺めて、これでも死ぬのかと思った。それで、ねんごろに枕の傍へ口を付けて、死ぬんじゃなかろうね、大丈夫だろうね、とまた聞き返した。すると女は黒い眼を眠そうにみはったまま、やっぱり静かな声で、でも、死ぬんですもの、仕方がないわと云った。"),
                  Text(
                      "じゃ、私の顔が見えるかいと一心に聞くと、見えるかいって、そら、そこに、写ってるじゃありませんかと、にこりと笑って見せた。自分は黙って、顔を枕から離した。腕組をしながら、どうしても死ぬのかなと思った。"),
                  Text("しばらくして、女がまたこう云った。"),
                  Text(
                      "「死んだら、埋めて下さい。大きな真珠貝で穴を掘って。そうして天から落ちて来る星の破片を墓標に置いて下さい。そうして墓の傍に待っていて下さい。また逢いに来ますから」"),
                ],
              ),
              MyBottomNavigation(
                count: _counter,
                increment: _increment,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
