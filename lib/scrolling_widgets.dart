import 'package:flutter/widgets.dart';

class ScrollingWidgets extends StatelessWidget {
  const ScrollingWidgets({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ScrollableIfNeededWithExpandedItemHorizontal();
    // return ScrollableIfNeededWithExpandedItem();
  }
}

class ScrollableIfNeededWithExpandedItemHorizontal extends StatelessWidget {
  const ScrollableIfNeededWithExpandedItemHorizontal({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Color.fromRGBO(255, 255, 255, 1),
      child: Directionality(
        textDirection: TextDirection.ltr,
        child: DefaultTextStyle(
          style: _myTextStyle,
          child: LayoutBuilder(
            builder: (
              BuildContext context,
              BoxConstraints viewportConstraints,
            ) {
              return SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: ConstrainedBox(
                  constraints: BoxConstraints(
                    minWidth: viewportConstraints.maxWidth,
                  ),
                  child: IntrinsicWidth(
                    child: _FlexibleBoxRow(),
                  ),
                ),
              );
            },
          ),
        ),
      ),
    );
  }
}

class ScrollableIfNeededWithExpandedItem extends StatelessWidget {
  const ScrollableIfNeededWithExpandedItem({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Color.fromRGBO(255, 255, 255, 1),
      child: Directionality(
        textDirection: TextDirection.ltr,
        child: DefaultTextStyle(
          style: _myTextStyle,
          child: LayoutBuilder(
            builder: (
              BuildContext context,
              BoxConstraints viewportConstraints,
            ) {
              return SingleChildScrollView(
                child: ConstrainedBox(
                  constraints: BoxConstraints(
                    minHeight: viewportConstraints.maxHeight,
                  ),
                  child: IntrinsicHeight(
                    child: _FlexibleBoxColumn(),
                  ),
                ),
              );
            },
          ),
        ),
      ),
    );
  }
}

class ScrollableIfNeededWithMinHeight extends StatelessWidget {
  const ScrollableIfNeededWithMinHeight({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Color.fromRGBO(255, 255, 255, 1),
      child: Directionality(
        textDirection: TextDirection.ltr,
        child: DefaultTextStyle(
          style: _myTextStyle,
          child: LayoutBuilder(
            builder: (
              BuildContext context,
              BoxConstraints viewportConstraints,
            ) {
              return SingleChildScrollView(
                child: ConstrainedBox(
                  constraints: BoxConstraints(
                    minHeight: viewportConstraints.maxHeight,
                  ),
                  child: _FixedBoxColumn(),
                ),
              );
            },
          ),
        ),
      ),
    );
  }
}

class ScrollableIfNeeded extends StatelessWidget {
  const ScrollableIfNeeded({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Color.fromRGBO(255, 255, 255, 1),
      child: Directionality(
        textDirection: TextDirection.ltr,
        child: DefaultTextStyle(
          style: _myTextStyle,
          child: SingleChildScrollView(
            child: _FixedBoxColumn(),
          ),
        ),
      ),
    );
  }
}

class _FlexibleBoxRow extends StatelessWidget {
  const _FlexibleBoxRow({
    Key key,
  }) : super(key: key);

  final _boxWidth = 180.0;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Container(
          color: Color.fromRGBO(255, 255, 0, 1),
          width: _boxWidth,
          alignment: Alignment.center,
          child: Text("Fixed Width Content"),
        ),
        Expanded(
          child: Container(
            color: Color.fromRGBO(0, 255, 255, 1),
            width: _boxWidth,
            alignment: Alignment.center,
            child: Text("Flexible Width Content"),
          ),
        ),
        Container(
          color: Color.fromRGBO(255, 0, 255, 1),
          width: _boxWidth,
          alignment: Alignment.center,
          child: Text("Fixed Width Content"),
        ),
      ],
    );
  }
}

class _FlexibleBoxColumn extends StatelessWidget {
  const _FlexibleBoxColumn({
    Key key,
  }) : super(key: key);

  final _boxHeight = 360.0;

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Container(
          color: Color.fromRGBO(255, 255, 0, 1),
          height: _boxHeight,
          alignment: Alignment.center,
          child: Text("Fixed Height Content"),
        ),
        Expanded(
          child: Container(
            color: Color.fromRGBO(0, 255, 255, 1),
            height: _boxHeight,
            alignment: Alignment.center,
            child: Text("Flexible Height Content"),
          ),
        ),
        Container(
          color: Color.fromRGBO(255, 0, 255, 1),
          height: _boxHeight,
          alignment: Alignment.center,
          child: Text("Fixed Height Content"),
        ),
      ],
    );
  }
}

class _FixedBoxColumn extends StatelessWidget {
  const _FixedBoxColumn({
    Key key,
  }) : super(key: key);

  final _boxHeight = 120.0;

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Container(
          color: Color.fromRGBO(255, 255, 0, 1),
          height: _boxHeight,
          alignment: Alignment.center,
          child: Text("Fixed Height Content"),
        ),
        Container(
          color: Color.fromRGBO(0, 255, 255, 1),
          height: _boxHeight,
          alignment: Alignment.center,
          child: Text("Fixed Height Content"),
        ),
        Container(
          color: Color.fromRGBO(0, 255, 255, 1),
          height: _boxHeight,
          alignment: Alignment.center,
          child: Text("Fixed Height Content"),
        ),
        Container(
          color: Color.fromRGBO(255, 0, 255, 1),
          height: _boxHeight,
          alignment: Alignment.center,
          child: Text("Fixed Height Content"),
        ),
      ],
    );
  }
}

// SingleChildScrollView > Column > Text
class MyExample extends StatelessWidget {
  const MyExample({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Color.fromRGBO(255, 255, 255, 1),
      child: Directionality(
        textDirection: TextDirection.ltr,
        child: DefaultTextStyle(
          style: _myTextStyle,
          child: SingleChildScrollView(
            child: _TextColumn(),
          ),
        ),
      ),
    );
  }
}

class _TextColumn extends StatelessWidget {
  const _TextColumn({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          "こんな夢を見た。",
        ),
        Text(
          "腕組をして枕元に坐っていると、仰向に寝た女が、静かな声でもう死にますと云う。女は長い髪を枕に敷いて、輪郭の柔らかな瓜実顔をその中に横たえている。真白な頬の底に温かい血の色がほどよく差して、唇の色は無論赤い。とうてい死にそうには見えない。しかし女は静かな声で、もう死にますと判然云った。自分も確にこれは死ぬなと思った。そこで、そうかね、もう死ぬのかね、と上から覗き込むようにして聞いて見た。死にますとも、と云いながら、女はぱっちりと眼を開けた。大きな潤のある眼で、長い睫に包まれた中は、ただ一面に真黒であった。その真黒な眸の奥に、自分の姿が鮮に浮かんでいる。",
        ),
        Text(
          "自分は透き徹るほど深く見えるこの黒眼の色沢を眺めて、これでも死ぬのかと思った。それで、ねんごろに枕の傍へ口を付けて、死ぬんじゃなかろうね、大丈夫だろうね、とまた聞き返した。すると女は黒い眼を眠そうにみはったまま、やっぱり静かな声で、でも、死ぬんですもの、仕方がないわと云った。",
        ),
        Text(
          "じゃ、私の顔が見えるかいと一心に聞くと、見えるかいって、そら、そこに、写ってるじゃありませんかと、にこりと笑って見せた。自分は黙って、顔を枕から離した。腕組をしながら、どうしても死ぬのかなと思った。",
        ),
        Text(
          "しばらくして、女がまたこう云った。",
        ),
        Text(
          "「死んだら、埋めて下さい。大きな真珠貝で穴を掘って。そうして天から落ちて来る星の破片を墓標に置いて下さい。そうして墓の傍に待っていて下さい。また逢いに来ますから」",
        ),
        Text(
          "自分は、いつ逢いに来るかねと聞いた。",
        ),
        Text(
          "「日が出るでしょう。それから日が沈むでしょう。それからまた出るでしょう、そうしてまた沈むでしょう。――赤い日が東から西へ、東から西へと落ちて行くうちに、――あなた、待っていられますか」",
        ),
        Text(
          "自分は黙って首肯いた。女は静かな調子を一段張り上げて、",
        ),
        Text(
          "「百年待っていて下さい」と思い切った声で云った。",
        ),
        Text(
          "「百年、私の墓の傍に坐って待っていて下さい。きっと逢いに来ますから」",
        ),
        Text(
          "自分はただ待っていると答えた。すると、黒い眸のなかに鮮に見えた自分の姿が、ぼうっと崩れて来た。静かな水が動いて写る影を乱したように、流れ出したと思ったら、女の眼がぱちりと閉じた。長い睫の間から涙が頬へ垂れた。――もう死んでいた。",
        ),
        Text(
          "自分はそれから庭へ下りて、真珠貝で穴を掘った。真珠貝は大きな滑かな縁の鋭どい貝であった。土をすくうたびに、貝の裏に月の光が差してきらきらした。湿った土の匂もした。穴はしばらくして掘れた。女をその中に入れた。そうして柔らかい土を、上からそっと掛けた。掛けるたびに真珠貝の裏に月の光が差した。",
        ),
        Text(
          "それから星の破片の落ちたのを拾って来て、かろく土の上へ乗せた。星の破片は丸かった。長い間大空を落ちている間に、角が取れて滑かになったんだろうと思った。抱き上げて土の上へ置くうちに、自分の胸と手が少し暖くなった。",
        ),
        Text(
          "自分は苔の上に坐った。これから百年の間こうして待っているんだなと考えながら、腕組をして、丸い墓石を眺めていた。そのうちに、女の云った通り日が東から出た。大きな赤い日であった。それがまた女の云った通り、やがて西へ落ちた。赤いまんまでのっと落ちて行った。一つと自分は勘定した。",
        ),
        Text(
          "しばらくするとまた唐紅の天道がのそりと上って来た。そうして黙って沈んでしまった。二つとまた勘定した。",
        ),
        Text(
          "自分はこう云う風に一つ二つと勘定して行くうちに、赤い日をいくつ見たか分らない。勘定しても、勘定しても、しつくせないほど赤い日が頭の上を通り越して行った。それでも百年がまだ来ない。しまいには、苔の生えた丸い石を眺めて、自分は女に欺されたのではなかろうかと思い出した。",
        ),
        Text(
          "すると石の下から斜に自分の方へ向いて青い茎が伸びて来た。見る間に長くなってちょうど自分の胸のあたりまで来て留まった。と思うと、すらりと揺ぐ茎の頂に、心持首を傾けていた細長い一輪の蕾が、ふっくらと弁を開いた。真白な百合が鼻の先で骨に徹えるほど匂った。そこへ遥の上から、ぽたりと露が落ちたので、花は自分の重みでふらふらと動いた。自分は首を前へ出して冷たい露の滴る、白い花弁に接吻した。自分が百合から顔を離す拍子に思わず、遠い空を見たら、暁の星がたった一つ瞬いていた。",
        ),
        Text(
          "「百年はもう来ていたんだな」とこの時始めて気がついた。",
        ),
      ],
    );
  }
}

final _myTextStyle = TextStyle(
  color: Color.fromRGBO(0, 0, 0, 0.81),
  fontWeight: FontWeight.bold,
  fontFamily: "Sans Serif",
  letterSpacing: 0.5,
  fontSize: 16,
  height: 2,
);
