import 'package:flutter/material.dart';

import 'app.dart';
import 'components/shopping_list.dart';
import 'constraints.dart';
import 'custom_scroll_view.dart';
import 'deal_with_box_constraints.dart';
import 'layout.dart';
import 'layouts_in_flutter.dart';
import 'models/product.dart';
import 'nested_scroll.dart';
import 'pavlova.dart';
import 'scrolling_widgets.dart';
import 'ten_dreaming_nights.dart';
import 'web_developers.dart';

void main() {
  runApp(
    NestedScroll(),
  );
}

// void main() {
//   runApp(
//     CustomScroll(),
//   );
// }

// void main() {
//   runApp(
//     ScrollingWidgets(),
//   );
// }

// void main() {
//   runApp(
//     DealWithBoxConstraints(),
//   );
// }

// void main() {
//   runApp(
//     WebDevelopers(),
//   );
// }

// void main() {
//   runApp(
//     Constraints(),
//   );
// }

// void main() {
//   runApp(
//     Pavlova(),
//   );
// }

// void main() {
//   runApp(
//     LayoutsInFlutter(),
//   );
// }

// void main() {
//   runApp(
//     Layout(),
//   );
// }

// void main() {
//   runApp(
//     TenDreamingNights(),
//   );
// }

// void main() {
//   runApp(
//     App(),
//   );
// }

// void main() {
//   runApp(
//     MaterialApp(
//       title: "Flutter Tutorial",
//       home: ShoppingList(
//         products: <Product>[
//           Product(name: 'Eggs'),
//           Product(name: 'Flour'),
//           Product(name: 'Chocolate chips'),
//         ],
//       ),
//     ),
//   );
// }
