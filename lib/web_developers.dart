import 'package:flutter/material.dart';

class WebDevelopers extends StatelessWidget {
  const WebDevelopers({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: TextDirection.ltr,
      child: Column(
        children: [
          Container(
            child: Center(
              child: Container(
                child: RichText(
                  text: TextSpan(
                    style: bold24Roboto,
                    children: <TextSpan>[
                      TextSpan(text: "Lorem "),
                      TextSpan(
                        text: "ipsum",
                        style: TextStyle(
                          fontWeight: FontWeight.w300,
                          fontStyle: FontStyle.italic,
                          fontSize: 48,
                        ),
                      ),
                    ],
                  ),
                ),
                decoration: BoxDecoration(
                  color: Colors.red[400],
                ),
                padding: EdgeInsets.all(16),
              ),
            ),
            width: 320,
            height: 240,
            color: Colors.grey[300],
          ),
          Container(
            child: Center(
              child: Container(
                child: Text(
                  "Lorem ipsum dolor sit amet, consec etur",
                  style: bold24Roboto,
                  overflow: TextOverflow.ellipsis,
                  maxLines: 1,
                ),
                decoration: BoxDecoration(
                  color: Colors.red[400],
                ),
                padding: EdgeInsets.all(16),
              ),
            ),
            width: 320,
            height: 240,
            color: Colors.grey[300],
          )
        ],
      ),
    );
  }
}

final TextStyle bold24Roboto = TextStyle(
  color: Colors.white,
  fontSize: 24,
  fontWeight: FontWeight.w900,
);
