import 'package:flutter/material.dart';

class Pavlova extends StatelessWidget {
  const Pavlova({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: TextDirection.ltr,
      child: Container(
        decoration: BoxDecoration(
          color: Color.fromRGBO(255, 255, 255, 1),
        ),
        child: _card,
      ),
    );
  }
}

final _card = Container(
  child: Card(
    child: Row(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        Container(
          width: 320,
          child: _leftColumn,
        ),
        _mainImage,
      ],
    ),
  ),
);

final _descTextStyle = TextStyle(
  color: Color.fromRGBO(0, 0, 0, 0.81),
  fontWeight: FontWeight.bold,
  fontFamily: "Sans Serif",
  letterSpacing: 0.5,
  fontSize: 16,
  height: 2,
);

final _iconList = DefaultTextStyle.merge(
  style: _descTextStyle,
  child: Container(
    padding: EdgeInsets.all(8),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        Column(
          children: [
            Icon(
              Icons.kitchen,
              color: Color.fromRGBO(0, 127, 0, 1),
            ),
            Text(
              "PREP:",
            ),
            Text(
              "25 min",
            ),
          ],
        ),
        Column(
          children: [
            Icon(
              Icons.timer,
              color: Color.fromRGBO(0, 127, 0, 1),
            ),
            Text(
              "COOK:",
            ),
            Text(
              "1 hr",
            ),
          ],
        ),
        Column(
          children: [
            Icon(
              Icons.restaurant,
              color: Color.fromRGBO(0, 127, 0, 1),
            ),
            Text(
              "FEEDS:",
            ),
            Text(
              "4-6",
            ),
          ],
        ),
      ],
    ),
  ),
);

final _leftColumn = Container(
  padding: EdgeInsets.fromLTRB(8, 16, 8, 8),
  child: Column(
    children: [
      _titleText,
      _subTitle,
      _ratings,
      _iconList,
    ],
  ),
);

final _mainImage = Expanded(
  child: FittedBox(
    fit: BoxFit.cover,
    alignment: Alignment.center,
    clipBehavior: Clip.hardEdge,
    child: Image.asset("images/pavlova.jpg"),
  ),
);

final _ratings = Container(
  padding: EdgeInsets.all(8),
  child: Row(
    mainAxisAlignment: MainAxisAlignment.spaceBetween,
    children: [
      _stars,
      Text(
        "170 Reviews",
        style: TextStyle(
          color: Color.fromRGBO(0, 0, 0, 0.81),
          fontWeight: FontWeight.bold,
          fontFamily: "Sans Serif",
          letterSpacing: 0.5,
          fontSize: 24,
        ),
      ),
    ],
  ),
);

final _stars = Row(
  mainAxisSize: MainAxisSize.min,
  children: [
    Icon(
      Icons.star,
      color: Color.fromRGBO(0, 127, 0, 1),
    ),
    Icon(
      Icons.star,
      color: Color.fromRGBO(0, 127, 0, 1),
    ),
    Icon(
      Icons.star,
      color: Color.fromRGBO(0, 127, 0, 1),
    ),
    Icon(
      Icons.star,
      color: Color.fromRGBO(0, 0, 0, 1),
    ),
    Icon(
      Icons.star,
      color: Color.fromRGBO(0, 0, 0, 1),
    ),
  ],
);

final _subTitle = Container(
  padding: EdgeInsets.all(8),
  child: Text(
    "Pavlova is a meringue-based dessert named after the Russian bellerine Anna Pavlova.\nPavlova featues a crisp crust and soft, light inside, topped with fruit and whipped cream.",
    style: TextStyle(
      color: Color.fromRGBO(0, 0, 0, 0.81),
      fontWeight: FontWeight.normal,
      fontFamily: "Sans Serif",
      letterSpacing: 0,
      fontSize: 16,
      height: 1.5,
    ),
  ),
);

final _titleText = Container(
  padding: EdgeInsets.all(8),
  child: Text(
    "Strawberry Pavlova",
    style: TextStyle(
      color: Color.fromRGBO(0, 0, 0, 0.81),
      fontWeight: FontWeight.bold,
      fontFamily: "Sans Serif",
      letterSpacing: 0.5,
      fontSize: 24,
      height: 1.5,
    ),
  ),
);
