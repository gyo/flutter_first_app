import 'package:flutter/material.dart';

class LayoutsInFlutter extends StatelessWidget {
  const LayoutsInFlutter({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Color.fromRGBO(255, 255, 255, 1),
      ),
      child: PackedImageColumnSample(),
    );
  }
}

class CenterTextSample extends StatelessWidget {
  const CenterTextSample({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Text(
        "Hello World",
        textDirection: TextDirection.ltr,
        style: TextStyle(
          fontSize: 32,
          color: Color.fromRGBO(0, 0, 0, 0.81),
        ),
      ),
    );
  }
}

class ImageColumnSample extends StatelessWidget {
  const ImageColumnSample({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      textDirection: TextDirection.ltr,
      children: [
        Image.asset("images/pic1.jpg"),
        Image.asset("images/pic2.jpg"),
        Image.asset("images/pic3.jpg"),
      ],
    );
  }
}

class ImageRowSample extends StatelessWidget {
  const ImageRowSample({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      textDirection: TextDirection.ltr,
      children: [
        Image.asset("images/pic1.jpg"),
        Image.asset("images/pic2.jpg"),
        Image.asset("images/pic3.jpg"),
      ],
    );
  }
}

class LargeImageRowSample extends StatelessWidget {
  const LargeImageRowSample({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      textDirection: TextDirection.ltr,
      children: [
        Expanded(
          child: Image.asset("images/pic1-l.jpg"),
        ),
        Expanded(
          flex: 2,
          child: Image.asset("images/pic2-l.jpg"),
        ),
        Expanded(
          child: Image.asset("images/pic3-l.jpg"),
        ),
      ],
    );
  }
}

class PackedImageColumnSample extends StatelessWidget {
  const PackedImageColumnSample({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Row(
        mainAxisSize: MainAxisSize.min,
        textDirection: TextDirection.ltr,
        children: [
          Image.asset("images/star-24px.png"),
          Image.asset("images/star-24px.png"),
          Image.asset("images/star-24px.png"),
          Image.asset("images/star-24px.png"),
          Image.asset("images/star-24px.png"),
        ],
      ),
    );
  }
}
