import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

final myDecoration = BoxDecoration(
  gradient: LinearGradient(
    begin: FractionalOffset.centerLeft,
    end: FractionalOffset.centerRight,
    colors: [
      Color.fromRGBO(255, 63, 63, 1),
      Color.fromRGBO(63, 63, 255, 1),
    ],
  ),
);

class Constraints extends StatelessWidget {
  const Constraints({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Example29Type2();
  }
}

// Scaffold の子を Scaffold と同じ大きさにするには、SizedBox.expand でラップする
class Example01 extends StatelessWidget {
  const Example01({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: myDecoration,
    );
  }
}

// Scaffold
class Example02 extends StatelessWidget {
  const Example02({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 100,
      height: 100,
      decoration: myDecoration,
    );
  }
}

// Expanded と同様 Row > Flexible の子は自身で width を決められない
// Flexible の子は Flexible よりも小さくなりうる
class Example03 extends StatelessWidget {
  const Example03({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        width: 100,
        height: 100,
        decoration: myDecoration,
      ),
    );
  }
}

// Row > Expanded の子は自身で width を決められない
class Example04 extends StatelessWidget {
  const Example04({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.bottomRight,
      child: Container(
        width: 100,
        height: 100,
        decoration: myDecoration,
      ),
    );
  }
}

class Example05 extends StatelessWidget {
  const Example05({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        width: double.infinity,
        height: double.infinity,
        decoration: myDecoration,
      ),
    );
  }
}

// Row > Expanded の子は自身で width を決められない
class Example06 extends StatelessWidget {
  const Example06({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        decoration: myDecoration,
      ),
    );
  }
}

// Row も UnconstrainedBox と同じ
// overflow warning!!
class Example07 extends StatelessWidget {
  const Example07({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        decoration: myDecoration,
        child: Container(
          width: 30,
          height: 30,
          color: Color.fromRGBO(127, 255, 127, 1),
        ),
      ),
    );
  }
}

// UnconstrainedBox と同じく Row も子に constraints を渡さない
class Example08 extends StatelessWidget {
  const Example08({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        decoration: myDecoration,
        padding: EdgeInsets.all(20),
        child: Container(
          width: 30,
          height: 30,
          color: Color.fromRGBO(127, 255, 127, 1),
        ),
      ),
    );
  }
}

class Example09 extends StatelessWidget {
  const Example09({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ConstrainedBox(
      constraints: BoxConstraints(
        minWidth: 70,
        minHeight: 70,
        maxWidth: 150,
        maxHeight: 150,
      ),
      child: Container(
        decoration: myDecoration,
      ),
    );
  }
}

// FittedBox は double.infinity を拡縮できない
class Example10 extends StatelessWidget {
  const Example10({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: ConstrainedBox(
        constraints: BoxConstraints(
          minWidth: 70,
          minHeight: 70,
          maxWidth: 150,
          maxHeight: 150,
        ),
        child: Container(
          width: 10,
          height: 10,
          decoration: myDecoration,
        ),
      ),
    );
  }
}

// Text は max: screen で折り返す
class Example11 extends StatelessWidget {
  const Example11({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: ConstrainedBox(
        constraints: BoxConstraints(
          minWidth: 70,
          minHeight: 70,
          maxWidth: 150,
          maxHeight: 150,
        ),
        child: Container(
          width: 1000,
          height: 1000,
          decoration: myDecoration,
        ),
      ),
    );
  }
}

// Example19 と同じ
// FittedBox は max: screen なので縮小する
class Example12 extends StatelessWidget {
  const Example12({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: ConstrainedBox(
        constraints: BoxConstraints(
          minWidth: 70,
          minHeight: 70,
          maxWidth: 150,
          maxHeight: 150,
        ),
        child: Container(
          width: 100,
          height: 100,
          decoration: myDecoration,
        ),
      ),
    );
  }
}

// Center は screen size
// FittedBox は min: 0, max: screen、子のサイズに合わせる
// Text は自分のサイズ
class Example13 extends StatelessWidget {
  const Example13({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return UnconstrainedBox(
      child: Container(
        width: 20,
        height: 50,
        decoration: myDecoration,
      ),
    );
  }
}

// FittedBox は子を constraints 無しで描画してから拡縮する
class Example14 extends StatelessWidget {
  const Example14({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return UnconstrainedBox(
      child: Container(
        width: 4000,
        height: 50,
        decoration: myDecoration,
      ),
    );
  }
}

// Center > ConstrainedBox
// Container は 100x100
class Example15 extends StatelessWidget {
  const Example15({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return OverflowBox(
      minWidth: 0,
      minHeight: 0,
      maxWidth: double.infinity,
      maxHeight: double.infinity,
      child: Container(
        width: 4000,
        height: 50,
        decoration: myDecoration,
      ),
    );
  }
}

// Center > LimitedBox
// Container は screenx100
// ？ LimitedBox の limit は infinite constraints を得たときのみ機能する
class Example16 extends StatelessWidget {
  const Example16({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return UnconstrainedBox(
      child: Container(
        width: double.infinity,
        height: 50,
        decoration: myDecoration,
      ),
    );
  }
}

// UnconstrainedBox > ConstrainedBox
// Container は 100x100
class Example17 extends StatelessWidget {
  const Example17({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return UnconstrainedBox(
      child: LimitedBox(
        maxWidth: 100,
        child: Container(
          width: double.infinity,
          height: 100,
          decoration: myDecoration,
        ),
      ),
    );
  }
}

// UnconstrainedBox > LimitedBox
// Container は 100x100
class Example17Type2 extends StatelessWidget {
  const Example17Type2({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return UnconstrainedBox(
      child: ConstrainedBox(
        constraints: BoxConstraints(
          maxWidth: 100,
        ),
        child: Container(
          width: double.infinity,
          height: 100,
          decoration: myDecoration,
        ),
      ),
    );
  }
}

// UnconstrainedBox とはいえ、double.infinity は描画できない
class Example17Type3 extends StatelessWidget {
  const Example17Type3({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: LimitedBox(
        maxWidth: 100,
        child: Container(
          width: double.infinity,
          height: 100,
          decoration: myDecoration,
        ),
      ),
    );
  }
}

// OverflowBox は UnconstrainedBox と違って overflow warning を表示しない
class Example17Type4 extends StatelessWidget {
  const Example17Type4({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: ConstrainedBox(
        constraints: BoxConstraints(
          maxWidth: 100,
        ),
        child: Container(
          width: double.infinity,
          height: 100,
          decoration: myDecoration,
        ),
      ),
    );
  }
}

// overflow warning!!
class Example18 extends StatelessWidget {
  const Example18({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FittedBox(
      child: Text(
        "Some Example Text.",
        textDirection: TextDirection.ltr,
      ),
    );
  }
}

// UnconstrainedBox は screen size
// Container には min: 0, max: screen を渡さない
class Example19 extends StatelessWidget {
  const Example19({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: FittedBox(
        child: Text(
          "Some Example Text.",
          textDirection: TextDirection.ltr,
        ),
      ),
    );
  }
}

// Example10 と同じ
class Example20 extends StatelessWidget {
  const Example20({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: FittedBox(
        child: Text(
          "This is some very very very large text that is too big to fit a regular screen in a single line.",
          textDirection: TextDirection.ltr,
        ),
      ),
    );
  }
}

// Example10 と同じ
class Example21 extends StatelessWidget {
  const Example21({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Text(
        "This is some very very very large text that is too big to fit a regular screen in a single line.",
        textDirection: TextDirection.ltr,
      ),
    );
  }
}

// Center は screen size
// ConstrainedBox は min: 0, max: screen
// Container は min: 0, max: screen ではなく、min: 70, max: 150
// Container は子を持たないので親のサイズに合わせる
class Example22 extends StatelessWidget {
  const Example22({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FittedBox(
      child: Container(
        width: double.infinity,
        height: 20,
        decoration: myDecoration,
      ),
    );
  }
}

// ？ ConstrainedBox は親から受け取る constraints に追加するだけ
class Example22Type2 extends StatelessWidget {
  const Example22Type2({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FittedBox(
      child: Container(
        width: 20,
        height: 20,
        decoration: myDecoration,
      ),
    );
  }
}

// Example07 と同じ
class Example23 extends StatelessWidget {
  const Example23({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      textDirection: TextDirection.ltr,
      children: [
        Container(
          decoration: myDecoration,
          child: Text(
            "Hello!",
            textDirection: TextDirection.ltr,
          ),
        ),
        Container(
          decoration: myDecoration,
          child: Text(
            "Goodbye!",
            textDirection: TextDirection.ltr,
          ),
        ),
      ],
    );
  }
}

// width, height を指定しない Container は、子のサイズに合わせる
class Example24 extends StatelessWidget {
  const Example24({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      textDirection: TextDirection.ltr,
      children: [
        Container(
          decoration: myDecoration,
          child: Text(
            "This is some very very very large text that is too big to fit a regular screen in a single line.",
            textDirection: TextDirection.ltr,
          ),
        ),
        Container(
          decoration: myDecoration,
          child: Text(
            "Goodbye!",
            textDirection: TextDirection.ltr,
          ),
        ),
      ],
    );
  }
}

// width, height を指定しない Container は可能な最大のサイズをとる
// これは Container widget の挙動
class Example25 extends StatelessWidget {
  const Example25({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      textDirection: TextDirection.ltr,
      children: [
        Expanded(
          child: Container(
            decoration: myDecoration,
            child: Text(
              "This is some very very very large text that is too big to fit a regular screen in a single line.",
              textDirection: TextDirection.ltr,
            ),
          ),
        ),
        Container(
          decoration: myDecoration,
          child: Text(
            "Goodbye!",
            textDirection: TextDirection.ltr,
          ),
        ),
      ],
    );
  }
}

// Example03 と同じ
// Container は double.infinity を要求しても親は max が screen なのでそのサイズになる
class Example25Type2 extends StatelessWidget {
  const Example25Type2({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      textDirection: TextDirection.ltr,
      children: [
        Expanded(
          child: Container(
            width: 200,
            height: 200,
            decoration: myDecoration,
          ),
        ),
        Container(
          decoration: myDecoration,
          child: Text(
            "Goodbye!",
            textDirection: TextDirection.ltr,
          ),
        ),
      ],
    );
  }
}

// Example03 と同じ
class Example26 extends StatelessWidget {
  const Example26({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      textDirection: TextDirection.ltr,
      children: [
        Expanded(
          child: Container(
            decoration: myDecoration,
            child: Text(
              "This is some very very very large text that is too big to fit a regular screen in a single line.",
              textDirection: TextDirection.ltr,
            ),
          ),
        ),
        Expanded(
          child: Container(
            decoration: myDecoration,
            child: Text(
              "Goodbye!",
              textDirection: TextDirection.ltr,
            ),
          ),
        ),
      ],
    );
  }
}

// screen は子である Center を同じサイズにする
// Center は min: 0, max: screen size を子に伝える
// 子は w100, h100 を Center に伝える
class Example27 extends StatelessWidget {
  const Example27({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      textDirection: TextDirection.ltr,
      children: [
        Flexible(
          child: Container(
            decoration: myDecoration,
            child: Text(
              "This is some very very very large text that is too big to fit a regular screen in a single line.",
              textDirection: TextDirection.ltr,
            ),
          ),
        ),
        Flexible(
          child: Container(
            decoration: myDecoration,
            child: Text(
              "Goodbye!",
              textDirection: TextDirection.ltr,
            ),
          ),
        ),
      ],
    );
  }
}

// screen は子を同じサイズにするので、Container に width, height を指定しても意味がない
class Example28 extends StatelessWidget {
  const Example28({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        body: Container(
          decoration: myDecoration,
          child: Column(
            children: [
              Text('Hello!'),
              Text('Goodbye!'),
            ],
          ),
        ),
      ),
    );
  }
}

// screen は子を同じサイズにする
class Example29 extends StatelessWidget {
  const Example29({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        body: SizedBox.expand(
          child: Container(
            decoration: myDecoration,
            child: Column(
              children: [
                Text('Hello!'),
                Text('Goodbye!'),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class Example29Type2 extends StatelessWidget {
  const Example29Type2({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        body: SizedBox.expand(
          child: Container(
            decoration: myDecoration,
            child: Text('Hello!'),
          ),
        ),
      ),
    );
  }
}
